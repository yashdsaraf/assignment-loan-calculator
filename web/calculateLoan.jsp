<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Loan Calculator</title>
<%@include file="header.jsp" %>

<h1>Your loan details are -- </h1>
<hr><br>
<style>
    p {
        font-family: sans-serif;
        font-weight: bold;
    }
</style>
<p>Loan interest type: ${param.type}</p>
<p>Principal amount: Rs. ${param.principal}</p>
<p>Rate of interest: ${param.roi}%</p>
<p>Period: ${param.period} years</p>
<%
    try {
        if (request.getParameter("type").equals("compound"))
            out.println("<p>No. of times interest is compounded annually: " +
                    request.getParameter("pa") + "</p>");
    } catch (Exception e) {
        // Do nothing
    }
%>
<p>Amount after ${param.period} years: Rs. ${amount}</p>
<p>Interest after ${param.period} years: Rs. ${interest}</p>
<%@include file="footer.jsp" %>
