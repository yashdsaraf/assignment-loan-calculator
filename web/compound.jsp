<%@page errorPage="error.jsp" %>
<%
    try {
        double principal = Double.parseDouble(request.getParameter("principal"));
        double period = Double.parseDouble(request.getParameter("period"));
        double roi = Double.parseDouble(request.getParameter("roi"));
        int pa = Integer.parseInt(request.getParameter("pa"));
        double amount, interest;

        amount = principal * Math.pow((1 + roi / pa), period);
        interest = amount - principal;
        request.setAttribute("interest", interest);
        request.setAttribute("amount", amount);
        request.getRequestDispatcher("calculateLoan.jsp").
                forward(request, response);
    } catch (Exception e) {
        request.setAttribute("error", e.getMessage());
        request.setAttribute("stacktrace", e.getStackTrace());
        request.getRequestDispatcher("error.jsp").
                forward(request, response);
    }

%>
