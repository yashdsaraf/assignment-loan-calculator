<%
    try {
        String type = request.getParameter("type");
        if (!type.isEmpty() && type.equals("simple")) {
            request.getRequestDispatcher("simple.jsp").
                    forward(request, response);
        } else if (!type.isEmpty() && type.equals("compound")) {
            request.getRequestDispatcher("compound.jsp").
                    forward(request, response);
        } else {
            throw new Exception();
        }
    } catch (Exception e) {
        request.setAttribute("error", "Invalid interest type selected");
        request.getRequestDispatcher("error.jsp").
                forward(request, response);
    }
%>
