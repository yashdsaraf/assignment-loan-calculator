<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Error!</title>
<%@include file="header.jsp" %>

<h2>Error:</h2>
<h3 style="color: red">
    ${error}!
</h3>
<h2>Stack trace:</h2>
<h5>
    <%
        for (StackTraceElement e:
                (StackTraceElement[])request.getAttribute("stacktrace"))
                out.println(e);
    %>
</h5>

<%@include file="footer.jsp" %>
