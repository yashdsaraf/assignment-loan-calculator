<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>
    footer {
        bottom: 1%;
        position: absolute;
    }
</style>
<footer>
    <h4>&copy Yash D. Saraf</h4>
    <jsp:useBean id="today" class="java.util.Date" scope="page" />
    <h5>Last updated on: ${today}</h5>
</footer>
