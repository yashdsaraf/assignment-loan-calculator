<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Loan Calculator</title>
<%@include file="header.jsp" %>

<h1>Please enter you loan details below:</h1>
<form action="controller.jsp" method="get">
    Principal: Rs. <input type="number" name="principal" required /> <br><br>
    Period: <input type="number" name="period" required /> years <br><br>
    Rate of interest: <input type="number" name="roi" required /> % <br><br>
    Interest calculation type:
    <input type="radio" name="type" value="simple" checked /> Simple
    <input type="radio" name="type" value="compound" /> Compound
    <br><br>
    No. of times interest is compounded per year:
    <input type="number" name="pa" value="0" required />
    <small>(has no effect if simple interest is selected)</small>
    <br><br>
    <input type="submit" value="Calulate">
    <input type="reset" value="Clear">
</form>

<%@include file="footer.jsp" %>

