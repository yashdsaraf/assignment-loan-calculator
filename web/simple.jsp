<%@page errorPage="error.jsp" %>
<%
    try {
        double principal = Double.parseDouble(request.getParameter("principal"));
        double period = Double.parseDouble(request.getParameter("period"));
        double roi = Double.parseDouble(request.getParameter("roi"));
        double amount, interest;

        interest = principal * period * roi / 100;
        amount = principal + interest;
        request.setAttribute("interest", interest);
        request.setAttribute("amount", amount);
        request.getRequestDispatcher("calculateLoan.jsp").
                forward(request, response);
    } catch (Exception e) {
        request.setAttribute("error", e.getMessage());
        request.setAttribute("stacktrace", e.getStackTrace());
        request.getRequestDispatcher("error.jsp").
                forward(request, response);
    }

%>
